$(document).ready(function() {
  $('button.open-button').click( function(event){
    event.preventDefault();
    $('#myOverlay').fadeIn(297,function(){
          $('#myForm')
          .css('display', 'block')
          .animate({opacity: 1}, 198);
    });
  });

  $('.closeForm').click(function() {
		$(this).parents('#myOverlay').fadeOut();
		return false;
	});

  $('#myOverlay').click(function(e) {
		if ($(e.target).closest('#myForm').length == 0) {
			$(this).fadeOut();
		}
	});
});

function onCheckboxCheck(event) {
    window.localStorage.setItem(event.target.id, event.target.checked);
    updateBtnSendState();
    console.log(window.localStorage);
}

function onChangeLocalStorageUpdater(event) {
    window.localStorage.setItem(event.target.id, event.target.value);
}

function onShownModal(e) {
    history.pushState({isShown: true}, "Form shown", "#formShown");
}

function onHiddenModal(e) {
    history.pushState({isShown: false}, "Form hidden", "");
}

function onSent() {
    let inputName = document.getElementById("name");
    inputName.value = "";
    window.localStorage.setItem(inputName.id, inputName.value);
    let inputMsg = document.getElementById("message");
    inputMsg.value = "";
    window.localStorage.setItem(inputMsg.id, inputMsg.value);
    let inputEmail = document.getElementById("emailAddress");
    inputEmail.value = "";
    window.localStorage.setItem(inputEmail.id, inputEmail.value);
    let checkboxAgree = document.getElementById("agreement");
    checkboxAgree.checked = false;
    window.localStorage.setItem(checkboxAgree.id, checkboxAgree.checked);
}

function onSend() {
    let ajaxData = {
        name: document.getElementById("name").value,
        message: document.getElementById("message").value,
        slap_replyto: document.getElementById("emailAddress").value
    };
    $.ajax({
        url: "https://api.slapform.com/tulia_87@mail.ru",
        dataType: "json",
        method: "POST",
        data: ajaxData,
        success: function (response) {
            console.log("Got data: ", response);
            if (response.meta.status === "success") {
                alert("Successful!");
            } else if (response.meta.status === "fail") {
                alert("Unsuccessful!");
            }
        }
    });
    onSent();
    return false;
}

window.addEventListener("DOMContentLoaded", function () {
    console.log(window.localStorage);

    let inputName = document.getElementById("name");
    let inputEmail = document.getElementById("emailAddress");
    let inputMsg = document.getElementById("message");
    let checkboxAgree = document.getElementById("agreement");

    inputName.value = window.localStorage.getItem(inputName.id);
    inputName.addEventListener("input", function(event) {
        return onChangeLocalStorageUpdater(event);});

    inputEmail.value = window.localStorage.getItem(inputEmail.id);
    inputEmail.addEventListener("input", function(event) {
        return onChangeLocalStorageUpdater(event);});

    inputMsg.value = window.localStorage.getItem(inputMsg.id);
    inputMsg.addEventListener("input", function(event) {
        return onChangeLocalStorageUpdater(event);});

    let checkboxStoredState = window.localStorage.getItem(checkboxAgree.id);
    if (checkboxStoredState === "true") {
        checkboxAgree.checked = true;
    } else {
        checkboxAgree.checked = false;
    }
    checkboxAgree.addEventListener("change", function(event) {
        return onCheckboxCheck(event);});

    $("#ajax_form").submit(function(){
        onSend();
        return false;
    });

    $("#myOverlay").on("shown.bs.modal", onShownModal);
    $("#myOverlay").on("hidden.bs.modal", onHiddenModal);

    
});


function click1() {
  let f1 = document.getElementsByName("field1");
  let f2 = document.getElementsByName("field2");
  f1[0].value = f1[0].value.replace(/[^\d+$]/g, '');
  f2[0].value = f2[0].value.replace(/[^\d+$]/g, '');
  let r = document.getElementById("result");
  r.innerHTML = parseInt(f1[0].value) * parseInt(f2[0].value);
  return false;
}

function itemChange(){
  document.getElementById('chosenItem').innerHTML = document.getElementById(document.getElementById('item').value).innerHTML

  let r = document.getElementById("price");
  if (document.getElementById("item").value == "item1"){
    r.innerHTML = 70;
  } else r.innerHTML = 0;
}

function click2(){
  let r = document.getElementById('price');
  let f1 = document.getElementById('item').value;
  if (f1 == 'item2'){
    if (document.getElementById('fries1').checked) r.innerHTML = 60;
    else if (document.getElementById('fries2').checked) r.innerHTML = 75;
    else if (document.getElementById('fries3').checked) r.innerHTML = 90;
  } else if (f1 == 'item3'){
    if (document.getElementById('meat').checked) r.innerHTML = 200;
    else if (document.getElementById('noMeat').checked) r.innerHTML = 150;
    if (document.getElementById('cheese').checked) r.innerHTML = parseInt(r.innerHTML) + 20;
  }
}

function click3(){
  let f1 = document.getElementById('price').value;
  let f2 = document.getElementsByName('amount');
  f2[0].value = f2[0].value.replace(/[^\d+$]/g, '');
  document.getElementById('total').innerHTML = parseInt(f1) * parseInt(f2[0].value);
  return false;
}

window.addEventListener("DOMContentLoaded", function(){
  $('.carousel').slick({
    dots: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
        slidesToShow: 3,
        }
      },
      {
        breakpoint: 769,
        settings: {
        slidesToShow: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
        slidesToShow: 2,
        }
      },
      {
        breakpoint: 330,
        settings: {
        slidesToShow: 1,
        }
      }

    ]
  });
});
